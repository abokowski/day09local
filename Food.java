public class Food
{
    private String description;
    
    private int calories;
    
    public int getCalories() {
        return calories;
    }
    
    public void setCalories(int inCalories) {
        calories = inCalories;
    }
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * Set the description
     * @param inDescription the new description of the food.
     */
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    
    @Override 
    public String toString() {
        return "Somebody brought" + getDescription();
    }
    
}